const nodemailer = require('nodemailer');
const emailService = require('../databases/email');
const server = "https://hobe-hobby.herokuapp.com/api/users/auth/";

const sendEmailTo = (email) =>{
    let transporter = nodemailer.createTransport(emailService.email_Service_Provider);
    let mailOptions = {
        from: 'Hobe App <hobe.hobbymail@gmail.com>',
        to: email,
        subject: 'Welcome to Hobe',
        text: 'Hello',
        html:"<a href='https://www.w3schools.com'>Visit W3Schools</a>"
    };
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: '+info);
        }
    });
    console.log('wew')
}

const sendWelcomeEmailTo = (email, name, activationCode) =>{
    let transporter = nodemailer.createTransport(emailService.email_Service_Provider);
    let mailOptions = {
        from: 'Hobe App <hobe.hobbymail@gmail.com>',
        to: email,
        subject: 'Welcome to Hobe',
        // text: 'Hello '+name+' and welcome to Hobe! the number one stop for your hobby needs. Feel free to use our app and as always, enjoy your stay!'
        html:"<p style='font-size:15px;'>Hello "+name+" and welcome to Hobe, the number one stop for your hobby needs.</p><p style='font-size:15px;'>You need to verify your account first before you can login to Hobe.</p><a style='font-size:15px;' href="+server+activationCode+">Click here to verify your account</s>"
    };
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Welcome Email sent to '+email);
        }
    });
}

module.exports ={
    sendEmailTo: sendEmailTo,
    sendWelcomeEmailTo: sendWelcomeEmailTo
}
// exports.sendEmailTo = sendEmailTo;
// exports.sendWelcomeEmailTo = sendWelcomeEmailTo;