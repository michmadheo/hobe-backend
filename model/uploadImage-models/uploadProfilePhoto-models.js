const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const profilePhotoImage = new Schema({
    profilePhoto: {type: String, required:true},
    createOn: {type:Number, required:true},
})

module.exports = mongoose.model('profilePhotoImage', profilePhotoImage);