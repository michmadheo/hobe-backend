const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const image = new Schema({
    image: {type: String, required:true},
    createOn: {type:Number, required:true},
})

module.exports = mongoose.model('image', image);