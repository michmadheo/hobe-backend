const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const topic = new Schema({
    title: {type: String, required:false},
})

module.exports = mongoose.model('topic', topic);