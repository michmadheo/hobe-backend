const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
    name: {type: String, required:true},
    username: {type: String, required:true},
    phone: {type: String, required:true},
    email: {type: String, required:true},
    password: {type: String, required:true},
    createOn: {type: Date, required:true},
    verified: {type:Boolean, required:true},
    token:{type: String, required:false},
    verificationId:{type:String, required:false},
    profilePhoto:{type:mongoose.Types.ObjectId, ref:'profilePhotoImage', required:false},
    likedTopic: [{type:mongoose.Types.ObjectId, ref:'topic', required:false}]
})

module.exports = mongoose.model('user', user);