const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const appdescription = new Schema({
    dev: {type: String, required:false},
    description: {type: String, required:false},
})

module.exports = mongoose.model('appdescription', appdescription);