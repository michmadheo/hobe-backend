const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const comment = new Schema({
    creator: {type: mongoose.Schema.Types.ObjectId, ref:'user', required:true},
    postId: {type: mongoose.Schema.Types.ObjectId, ref: 'comment', required:false},
    content: {type: String, required:true},
    createOn: {type:Number, required:true},
    comments: [{type:String, required:false}]
})

module.exports = mongoose.model('comment', comment);