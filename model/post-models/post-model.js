const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const post = new Schema({
    creator: {type: mongoose.Schema.Types.ObjectId, ref:'user', required:true},
    title: {type:String, required:true},
    content: {type: String, required:true},
    createOn: {type:Number, required:true},
    image:{type:mongoose.Types.ObjectId, ref:'image', required:false},
    topic: {type: mongoose.Schema.Types.ObjectId, ref:'topic', required:true},
    comments: [{type:mongoose.Types.ObjectId, ref:'comment', required:false}]
})

module.exports = mongoose.model('post', post);