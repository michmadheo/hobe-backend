const date = new Date();
const express = require('express');
const mongoose = require('mongoose')
const bodyParser = require('body-parser');
const routes = require('./routes/api-routes');
const database = require('./databases/database');


const app = express();

app.use(bodyParser.json({limit:'50mb'}));

app.get('/',(req,res,next)=>{
    res.json({
        message:"Hello, you're connected to Hobe App API endpoint, read the docs for more awesome APIs!"
    })
})

app.use('/api', routes)

mongoose.connect(database.databaseHobe, {useNewUrlParser:true, useUnifiedTopology:true}).then(()=>{
    console.log('Server & Database connection established at '+date)
    app.listen(process.env.PORT || 5000);
})
.catch((err)=>{
    console.log(err)
})
