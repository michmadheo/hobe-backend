const express = require('express');
const appinfo = require('../controllers/appinfo-controllers');
const users = require('../controllers/users-controllers');
const search = require('../controllers/search-controllers');
const post = require('../controllers/post-controllers');
const upload = require('../controllers/upload-controller');
const fileUpload = require('../functions/uploadImage');

const router = express.Router()

router.post('/appinfo/appdescription', appinfo.appdescription);

router.post('/users/register', users.register);
router.post('/users/login', users.login);
router.post('/users/autoLoginCheck', users.autoLoginCheck);
router.get('/users/auth/:uid', users.verifyAccount);

router.post('/topics/allTopics', search.topics);
router.post('/items/searchAll', search.searchAll);

router.post('/userAction/postNew', post.postNew);
router.post('/userAction/editPost', post.editPost);
router.post('/userAction/deletePost', post.deletePost);
router.post('/userAction/postComment', post.postComment);
router.post('/userAction/postReply', post.postReply);

router.post('/post/getPost', post.getPost);
router.post('/post/getSinglePost', post.getSinglePost);

router.post('/upload/uploadProfilePhoto', upload.uploadProfilePhoto);
router.get('/loadProfilePhoto/:id', upload.loadPhotoProfile);
router.get('/loadImage/:id', upload.loadImage);

module.exports = router;