const User = require('../model/user-models/user-model');
const Post = require('../model/post-models/post-model');
const CommentPost = require('../model/post-models/comment-model')
const UploadPhoto = require('../model/uploadImage-models/uploadImage-models');
const Topic = require('../model/topics-model');
const nodemailer = require('nodemailer');
const emailService = require('../databases/email');
const sendEmail = require('../functions/sendEmail');
const mongoose = require('mongoose');

const postNew = async (req,res,next)=>{
    const {username, title, content, topic, image} = req.body 
    const date = new Date().getTime()
    try{
        const user = await User.findOne({username:username},{username:1})
        if(user){
            let request = ""
            if(image){
                let decodedImage = Buffer.from(image, 'base64').toString('binary')
                const newImage = new UploadPhoto({
                    image: decodedImage,
                    createOn: date
                })
                await newImage.save()
                imageId = await newImage._id
                request ={
                    creator:user._id,
                    topic:topic,
                    title:title,
                    content:content,
                    createOn:date,
                    image: newImage._id,
                    comments:[]
                }
            }
            else{
                request = {
                    creator:user._id,
                    topic:topic,
                    title:title,
                    content:content,
                    createOn:date,
                    comments:[]
                }
            }
            // const findTopic = await Topic.findOne({_id:topic})
            const newPost = new Post(request);
            await newPost.save();
            res.status(200)
            res.json({
                status:"SUCCESS",
                message:"Successfully posted!",
                data: newPost,
            })
        }
        else{
            res.status(500)
            res.json({
                status:"ERROR",
                message:"User Not Found"
            })
        }
    }
    catch(err){
        res.status(500)
        res.json({
            status:"ERROR",
            message:JSON.stringify(err)
        })
    }
}

const editPost = async (req,res,next)=>{
    const {username, post_id, title, content} = req.body
    const user = await User.findOne({username:username},{username:1})
    if(user){
        try{
            const allPost = await Post.findOne({_id:post_id}, {title:1, content:1}).exec();
            allPost.title = title
            allPost.content = content
            await allPost.save()
            res.status(200)
            res.json({
                status:"SUCCESS",
                message:"Post Edited"
            })
        }
        catch(err){
            res.status(500)
            res.json({
                status:"ERROR",
                message:"Not Found",
            })
        }
    }
    else{
        res.status(500)
        res.json({
                status:"ERROR",
                message:"Username not found",
            })
    }
}

const deletePost = async (req,res,next)=>{
    const {username, post_id} = req.body
    const user = await User.findOne({username:username},{username:1})
    if(user){
        try{
            const allPost = await Post.findOne({_id:post_id}, {title:1, content:1}).exec();
            await allPost.remove()
            res.status(200)
            res.json({
                status:"SUCCESS",
                message:"Post Deleted"
            })
        }
        catch(err){
            res.status(500)
            res.json({
                status:"ERROR",
                message:"Post not found",
            })
        }
    }
    else{
        res.status(500)
        res.json({
                status:"ERROR",
                message:"Username not found",
            })
    }
}

const postComment = async (req,res,next)=>{
    const {username, post_id, comment} = req.body
    const user = await User.findOne({username:username},{username:1})
    if(user){
        try{
            // const sess = await mongoose.startSession();
            // sess.startTransaction();
            const date = new Date().getTime();
            const allPost = await Post.findOne({_id:post_id},{comments:1}).exec();
            if(allPost){
                const newComment = new CommentPost({
                    creator:user._id,
                    // postId: post_id,
                    content: comment,
                    createOn: date,
                    comments: []
                });
                await newComment.save();
                allPost.comments.push(newComment._id);
                await allPost.save();
                res.status(200)
                res.json({
                    status:"SUCCESS",
                    message:"Posted",
                })
            }
            else{
                res.status(200)
                res.json({
                    status:"ERROR",
                    message:"This post is no longer available",
                })
            }
        }
        catch(err){
            res.status(500)
            res.json({
                status:"ERROR",
                message:"Not Found",
            })
        }
    }
    else{
        res.status(500)
        res.json({
                status:"ERROR",
                message:"Username not found",
            })
    }
}

const postReply = async (req,res,next)=>{
    const {username, post_id, comment} = req.body
    const user = await User.findOne({username:username},{username:1})
    if(user){
        try{
            // const sess = await mongoose.startSession();
            // sess.startTransaction();
            const date = new Date().getTime();
            const allPost = await CommentPost.findOne({_id:post_id},{comments:1}).exec();
            const newComment = new CommentPost({
                creator:user._id,
                postId: post_id,
                content: comment,
                createOn: date,
            });
            await newComment.save();
            allPost.comments.push(newComment._id);
            await allPost.save();
            res.status(200)
            res.json({
                status:"SUCCESS",
                message:allPost,
            })
        }
        catch(err){
            res.status(500)
            res.json({
                status:"ERROR",
                message:"Not Found",
            })
        }
    }
    else{
        res.status(500)
        res.json({
                status:"ERROR",
                message:"Username not found",
            })
    }
}

const getPost = async (req,res,next)=>{
    const {username, page, firstData} = req.body
    const user = await User.findOne({username:username},{username:1})
    let pagingFront = page*10-1;
    let pagingBack = pagingFront+10
    if(pagingFront<0){
        pagingFront = 0
    }
    if(user){
        let allPost;
        if(page===0){
            allPost = await Post.find().populate('creator', 'username profilePhoto').populate('topic', 'title').sort({createOn: -1}).exec()
        }
        else{
            allPost = await Post.find({"createOn":{$lt:firstData}}).populate('creator', 'username profilePhoto').populate('topic', 'title').sort({createOn: -1}).exec()
        }
        try{
            if(pagingBack > allPost.length){
                pagingBack = allPost.length
            }
            const sendPost = allPost.slice(pagingFront,pagingBack)
            if(sendPost.length>0){
                res.status(200)
                res.json({
                    status:"SUCCESS",
                    message:"",
                    data: sendPost
                })
            }
            else{
                res.status(200)
                res.json({
                    status:"MAXIMUM",
                    message:"You've reached all the posts",
                })
            }
        }
        catch(err){
            res.status(500)
            res.json({
                status:"ERROR",
                message:"Not Found",
            })
        }
    }
    else{
        res.status(500)
        res.json({
                status:"ERROR",
                message:"Not Found",
            })
    }
}

const getSinglePost = async (req,res,next)=>{
    const {username, id} = req.body
    const user = await User.findOne({username:username},{username:1})
    if(user){
        try{
            const allPost = await Post.findOne({_id:id})
            .populate('creator', 'username profilePhoto')
            .populate('topic', 'title')
            .populate({
                path:'comments',
                populate: [{
                    path: 'creator',
                    model: 'user',
                    select: 'username profilePhoto'
                },{
                    path: 'comments',
                    populate: {
                        path: 'creator',
                        model: 'user',
                        select: 'username profilePhoto'
                    }
                }]
            })
            .sort({createOn: -1})
            .exec()
            if(allPost){
                res.status(200)
                res.json({
                    status:"SUCCESS",
                    message:"",
                    data: allPost,
                })
            }
            else{
                res.status(200)
                res.json({
                    status:"ERROR",
                    message:"This post is no longer available"
                })
            }
        }
        catch(err){
            res.status(500)
            res.json({
                status:"ERROR",
                message:"Not Found",
            })
        }
    }
    else{
        res.status(500)
        res.json({
                status:"ERROR",
                message:"Not Found",
            })
    }
}

exports.postNew = postNew;
exports.editPost = editPost;
exports.deletePost = deletePost;
exports.postComment = postComment;
exports.postReply = postReply;
exports.getPost = getPost;
exports.getSinglePost = getSinglePost;