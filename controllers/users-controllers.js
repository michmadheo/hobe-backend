const date = new Date()
const bcrypt = require('bcryptjs');
const User = require('../model/user-models/user-model');
const jwt = require('jsonwebtoken');
const uuid = require('uuid');

const sendEmail = require('../functions/sendEmail');

const login = async(req,res,next)=>{
    const {username,password} = await req.body
    let userData;
    try{
        if(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(username)){
            userData = await User.findOne({email:username});
        }
        else{
            userData = await User.findOne({username:username});
        }
    }
    catch(err){
        res.status(200)
            res.json({
            status:"FAILED",
            message:"Username not found",
            data: {
            }
        })
        console.log('Login Failed username not found at '+date) 
    }
    try{
        // const userData = await User.findOne({username:username});
        if(userData){
            let validPassword = false;
            validPassword = await bcrypt.compare(password, userData.password)
            if(validPassword){
                if(userData.verified === true){
                    let token;
                    try{
                        token = await jwt.sign({userId: userData.id, email: userData.email}, uuid.v4())
                    }
                    catch(err){
                    }
                    userData.token = token;
                    await userData.save();
                    res.status(200)
                    res.json({
                        status:"SUCCESS",
                        message:"Login Succesful",
                        data: {
                            name: userData.name,
                            username: userData.username,
                            email: userData.email,
                            phone: userData.phone,
                            token: userData.token,
                            id: userData._id,
                            profilePhoto: userData.profilePhoto
                        }
                    })
                    console.log('New Login -Username: '+username+' Logged at '+date)
                }
                else{
                    res.status(200)
                    res.json({
                        status:"FAILED",
                        message:"Please verify your email ",
                        data: {
                        }
                    })
                    console.log('Login Failed unverified email -Username: '+username+' Logged at '+date) 
                }
            }
            else{
                res.status(200)
                res.json({
                    status:"FAILED",
                    message:"Incorrect Password",
                    data: {
                    }
                })
                console.log('Login Failed incorrect password -Username: '+username+' Logged at '+date) 
            }
        }
        else{
            res.status(200)
                res.json({
                    status:"FAILED",
                    message:"Username not found",
                    data: {
                    }
                })
                console.log('Login Failed username not found at '+date) 
        }
    }
    catch(err){
        res.status(500)
        res.json({
            status:"FAILED",
            message:"Login Failed",
            errorMsg:err
        })
        console.log('Login Failed at '+date)
    }
}

const autoLoginCheck = async(req,res,next)=>{
    const {username, token} = await req.body
    let userData;
    try{
        userData = await User.findOne({username:username});
        if(userData){
            if(userData.token === token){
                res.status(200)
                res.json({
                    status:"SUCCESS",
                    message:"Login Succesfulsss",
                    data: {
                        name: userData.name,
                        username: userData.username,
                        email: userData.email,
                        phone: userData.phone,
                        token: userData.token,
                        id: userData._id,
                        profilePhoto: userData.profilePhoto
                    }
                })
            }
            else{
                res.status(200)
                res.json({
                    status:"FAILED",
                    message:"You are logged on another device",
                    data: {
                    }
                })
            }
        }
        else{
            res.status(200)
                res.json({
                    status:"FAILED",
                    message:"Username not found",
                    data: {
                    }
                })
        }
    }
    catch(err){
        res.status(500)
        res.json({
            status:"FAILED",
            message:"Login Failed",
            errorMsg:err
        })
    }
}

const register = async(req,res,next)=>{
    const {name,username,phone,email,password} = await req.body
    try{
        let hashedPassword = await bcrypt.hash(password, 12);
        const checkUsername = await User.findOne({username:username}).exec()
        const checkEmail = await User.findOne({email:email}).exec()
        if(checkUsername === null && checkEmail === null){
            const verificationId = await uuid.v4()
            const newUser = new User({
                name:name,
                username:username,
                phone:phone,
                email:email,
                password:hashedPassword,
                createOn:date,
                verified:false,
                token:"abc",
                verificationId:verificationId,
                likedTopic:[]
            });
            await newUser.save();
            res.status(201)
            res.json({
                status:"SUCCESS",
                message:"Welcome aboard! we've sent a verification email for you. you can login once your email has been verified",
            })
            console.log('New account -Name: '+name+' -Username: '+username+' registered at '+date)
            sendEmail.sendWelcomeEmailTo(email, name, verificationId);
        }
        else{
            let takenEmail = ""
            let takenUsername = ""
            if(checkEmail !== null){
                takenEmail  = "Email already taken"
            }
            if(checkUsername !== null){
                takenUsername  = "Username already taken"
            }
            res.status(200)
            res.json({
                status:"TAKEN",
                messageEmail:takenEmail,
                messageUsername: takenUsername
            })
        }
    }
    catch(err){
        res.status(500)
        res.json({
            status:"FAILED",
            message:"Register Failed",
            errorMsg:err
        })
        console.log('Register Failed at '+date)
    }
}

const verifyAccount = async (req,res,next)=>{
    const verificationId = req.params.uid
    const userData = await User.findOne({verificationId:verificationId}).exec()
    try{
        if(userData){
            userData.verificationId = ""
            userData.verified = true;
            await userData.save()
            res.end("<p style='text-align: center; font-size:20px;'>Your account has been verified successfully. you can now login to Hobe, "+userData.name+"</p>")
        }
        else{
            res.end("<p style='text-align: center; font-size:20px;'>This link is no longer accessible</p>")
        }
    }
    catch(err){
        res.end("WEW")
    }
}

exports.register = register;
exports.login = login;
exports.verifyAccount = verifyAccount;
exports.autoLoginCheck = autoLoginCheck;