const User = require('../model/user-models/user-model');
const UploadPhoto = require('../model/uploadImage-models/uploadProfilePhoto-models');
const UploadImage = require('../model/uploadImage-models/uploadImage-models');
const nodemailer = require('nodemailer');
const emailService = require('../databases/email');
const convertImages = require('../functions/convertImage');
const mongoose = require('mongoose');

const uploadProfilePhoto = async (req,res,next)=>{
    const {username, photoData} = req.body
    const date = new Date().getTime()
    try{
        const userData = await User.findOne({'username':username}, {username:1, profilePhoto:1}).exec()
        if(userData && photoData){
            try{
                let decodedImage = Buffer.from(photoData, 'base64').toString('binary')
                const newProfilePhoto = await new UploadPhoto({
                    profilePhoto: decodedImage,
                    createOn: date
                })
                await newProfilePhoto.save()
                userData.profilePhoto = newProfilePhoto._id;
                await userData.save()
                res.status(200)
                res.json({
                    status:"SUCCESS",
                    message:"Profile photo successfully changed",
                    data:newProfilePhoto._id
                })
                // console.log(decode)
            }
            catch(err){
                res.status(500)
                res.json({
                status:"ERROR",
                message:"Error uploading Image",
                })
                console.log(err) 
            }
        }
        else{
            res.status(500)
            res.json({
                status:"ERROR",
                message:"Not Found",
            })
            console.log('nope') 
        }
    }
    catch(err){
        res.status(500)
        res.json({
            status:"ERROR",
            message:"Not Found",
        })
        console.log('nope')
    }
    // try{
        // res.status(200)
        // res.json({
        //     status:"SUCCESS",
        //     message:"",
        //     data: "wii"
        // })
        // console.log('sukses')
    // }
    // catch(err){
    //     res.status(500)
    //     res.json({
    //         status:"ERROR",
    //         message:"Not Found",
    //     })
    //     console.log('nope')
    // }
}

// const uploadProfilePhoto = async (req,res,next)=>{
//     const {username, photoData} = req.body
//     try{
//         const userData = await User.findOne({'username':username}, {username:1, profilePhoto:1}).exec()
//         if(userData && photoData){
//             try{

//                 let decode = Buffer.from(photoData, 'base64').toString('binary')
//                 // let encode = Buffer.from(decode, 'binary').toString('base64')
//                 userData.profilePhoto = photoData;
//                 await userData.save()
//                 res.status(200)
//                 res.json({
//                     status:"SUCCESS",
//                     message:"Profile photo successfully changed",
//                     data:photoData
//                 })
//                 // console.log(decode)
//             }
//             catch(err){
//                 res.status(500)
//                 res.json({
//                 status:"ERROR",
//                 message:"Error uploading Image",
//                 })
//                 console.log(err) 
//             }
//         }
//         else{
//             res.status(500)
//             res.json({
//                 status:"ERROR",
//                 message:"Not Found",
//             })
//             console.log('nope') 
//         }
//     }
//     catch(err){
//         res.status(500)
//         res.json({
//             status:"ERROR",
//             message:"Not Found",
//         })
//         console.log('nope')
//     }
//     // try{
//         // res.status(200)
//         // res.json({
//         //     status:"SUCCESS",
//         //     message:"",
//         //     data: "wii"
//         // })
//         // console.log('sukses')
//     // }
//     // catch(err){
//     //     res.status(500)
//     //     res.json({
//     //         status:"ERROR",
//     //         message:"Not Found",
//     //     })
//     //     console.log('nope')
//     // }
// }

const loadPhotoProfile = async (req,res,next)=>{
    const imageId = req.params.id
    try{
        // const userImage = await User.findOne({'username':'michmadheo'}, {profilePhoto:1}).exec()
        const userImage = await UploadPhoto.findOne({'_id':imageId}, {profilePhoto:1}).exec()
        let encodedImage = Buffer.from(userImage.profilePhoto, 'binary').toString('base64')
        var img = Buffer.from(encodedImage, 'base64');
        res.writeHead(200,{
            'Content-Type': 'image/png',
            'Content-Length': img.length
        });
        res.end(img);
        // res.json(encodedImage)
    }
    catch(err){
        res.json({
            status:"ERROR",
            message: "Could not retrieve image"
        })
    }
}

const loadImage = async (req,res,next)=>{
    const imageId = req.params.id
    try{
        // const userImage = await User.findOne({'username':'michmadheo'}, {profilePhoto:1}).exec()
        const userImage = await UploadImage.findOne({'_id':imageId}, {image:1}).exec()
        let encodedImage = await Buffer.from(userImage.image, 'binary').toString('base64')
        var img = Buffer.from(encodedImage, 'base64');
        res.writeHead(200,{
            'Content-Type': 'image/png',
            'Content-Length': img.length
        });
        res.end(img);
        // res.json(encodedImage)
    }
    catch(err){
        res.json({
            status:"ERROR",
            message: "Could not retrieve image"
        })
    }
}

exports.uploadProfilePhoto = uploadProfilePhoto;
exports.loadPhotoProfile = loadPhotoProfile;
exports.loadImage = loadImage;