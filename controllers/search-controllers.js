const date = new Date()
const Topic = require('../model/topics-model');

const topics = async (req,res,next)=>{
    try{
        let topic = await Topic.find().sort({title:1})
        res.json({
            status:"SUCCESS",
            message:"",
            data: topic
        })
        res.status(200)
        // console.log('All topics retrieved at '+date)
    }
    catch(err){

    }
}

const searchAll = async (req,res,next)=>{
    try{
        let searchReq = await req.body.search;
        var searchValue = new RegExp('^'+req.body.search, "i");
        let topic = await Topic.find({title:{'$regex': searchValue}})
        res.json({
            status:"SUCCESS",
            data:{
                topic:topic
            }
        })
    }
    catch(err){
        res.status(500)
        res.json({
            status:"FAILED",
            message:"Search Error",
            errorMsg:err
        })
        console.log('Search Failed at '+date)
    }
}

exports.topics = topics;
exports.searchAll = searchAll;