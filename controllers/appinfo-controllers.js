const date = new Date()
const Info = require('../model/appinfo-model');
const nodemailer = require('nodemailer');
const emailService = require('../databases/email');
const sendEmail = require('../functions/sendEmail');

const appdescription = async (req,res,next)=>{
    let info = await Info.findOne().exec()
    res.json({
        status:"SUCCESS",
        message:"",
        data: info
    })
    console.log('Application Info retrieved at '+date)
    // sendEmail.sendWelcomeEmailTo('michaelamadheo@gmail.com', 'Michael');
}

exports.appdescription = appdescription;